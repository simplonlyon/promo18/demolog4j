# Démo de Log4J2

1. Créer un projet Java classique co.simplon.p18.demolog4j avec maven, en utilisant l'achétype "quickstart"
2. Ajouter Log4j à votre projet
3. Dans le main, écrire un log de niveau Error sur la console, avec le message "Bienvenue dans le monde de Log4J"
4. Créer une classe Dog, ajouter une méthode "bark()", et logguer en mode info que le chien a aboyé
5. Créer une méthode feed(String food), et logguer en mode info que vous avez nourri le chien, et en mode debug le type de nourriture
6. Modifier la configuration de log4j pour faire apparaître ces éléments dans la console (puisqu'au départ il n'affiche que les warnings ou supérieur)
    * Créer un dossier `resources` dans le dossier `main`, y placer un fichier nommé `log4j2.xml`, et écrire dans ce fichier la configuration de log4j
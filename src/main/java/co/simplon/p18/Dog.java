package co.simplon.p18;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Dog {

    private static final Logger logger =  LogManager.getLogger();
    
    public void bark() {
        System.out.println("Wouf");
        logger.info("Le chien a bien aboyé");
    }

    public void feed(String food) {
        logger.info("Le chien a été nourri");
        logger.debug("  Il a mangé [" + food + "]");
    }
}

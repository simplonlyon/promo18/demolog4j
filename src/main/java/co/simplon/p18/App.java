package co.simplon.p18;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger logger =  LogManager.getLogger();
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        logger.error( "Bienvenue dans le monde de Log4J" );

        Dog d = new Dog();
        d.bark();
        d.feed("boulettes de boeuf");

            /* Ordre des sévérités :
		logger.trace("It is a trace logger.");
        logger.debug("It is a debug logger.");
		logger.info("It is a info logger.");
		logger.warn("It is a warn logger.");
		logger.error("It is an error logger.");
		logger.fatal("It is a fatal logger.");
         */
    }
}
